import React, { createContext, useEffect, useReducer } from 'react';
import { useContext } from 'react';

export const quizContext = createContext();
const SEC_PER1_QUESTIONS = 1;
const initialState = {
    questions: [],
    // ready,error,finished,next,active
    status: "loading",
    index: 0,
    point: 0,
    answer: null,
    secondRemaining: 0
}
function reducer(state, action) {
    switch (action.type) {
        case "dataReceive":
            return {
                ...state,
                questions: action.payload,
                status: "ready"
            }
        case "error":
            return {
                ...state,
                questions: action.payload,
                status: "error"
            }
        case "start":
            return {
                ...state,
                status: "active",
                secondRemaining: state.questions.length * SEC_PER1_QUESTIONS
            }
        case "newAnswer":
            const question = state.questions.at(state.index);
            return {
                ...state,
                answer: action.payload,
                point: action.payload == question.correctOption ? state.point + question.points : (state.point),
            }
        case "nextQuestions":
            return {
                ...state,
                index: state.index + 1,
                answer: null
            }
        case "Restart":
            return {
                ...initialState,
                questions: state.questions,
                status: "ready"
            }
        case "finished":
            return {
                ...state,
                status: "finish"
            }
        case "tick":
            return {
                ...state,
                secondRemaining: state.secondRemaining - 1,
                status: state.secondRemaining == 0 ? "finish" : state.status
            }
        default: throw new Error("This action is undefined")
    }
}

const QuizContextProvider = ({ children }) => {
    const [{ questions, status, index, point, answer, secondRemaining }, dispatch] = useReducer(reducer, initialState)
    // console.log(status);
    const numberOfQuestions = questions.length;
    let maxPossiblePoints = questions.reduce((prev, cur) => prev + cur.points, 0);
   
    useEffect(() => {
        fetch("http://localhost:8080/questions")
            .then((res) => res.json())
            .then((data) => dispatch({ type: "dataReceive", payload: data }))
            .catch((err) => dispatch({ type: "error", payload: err.massage }))
    }, [])
    return (
        <quizContext.Provider value={{numberOfQuestions,maxPossiblePoints,questions,index,dispatch,answer,secondRemaining,point,status}}>
            {children}
        </quizContext.Provider>
    )
}
export function useQuiz(){
    return useContext(quizContext)
}
export default QuizContextProvider;