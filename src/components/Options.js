import React, { useContext } from 'react'
import "../index.css"
import { quizContext, useQuiz } from '../context/QuizContextProvider';
function Options({exactlyQuestion}) {
    const {dispatch,answer}=useQuiz();
    const hasAnswer = answer != null;
    return (
        <div className='options'>
            {
                exactlyQuestion?.options.map((option, index) =>
                (<button
                    className={`btn btn-option ${answer == index ? "answer" : ""} ${hasAnswer ? index == exactlyQuestion.correctOption ? "correct" : "wrong":""}`}
                    key={index}
                    onClick={() => dispatch({ type: "newAnswer", payload: index })}>{option}</button>))
            }
        </div>
    )
}

export default Options


