import React, { useContext } from 'react'
import Options from './Options';
import { quizContext, useQuiz } from '../context/QuizContextProvider';
function Questions() {
    const { questions, index } = useQuiz()
    const exactlyQuestion = questions[index];
    return (
        <div>
            <h4 style={{ width: "500px" }}>{exactlyQuestion.question}</h4>
            <Options exactlyQuestion={exactlyQuestion}/>
        </div>
    )
}

export default Questions

