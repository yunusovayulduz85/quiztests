import React, { useContext, useEffect } from 'react'
import { quizContext, useQuiz } from '../context/QuizContextProvider';

const Timer = () => {
  const { secondRemaining, dispatch } = useQuiz()
  let mins = Math.floor(secondRemaining / 60);
  let sec = secondRemaining % 60;

  useEffect(() => {
    let id = setInterval(() => {
      dispatch({ type: "tick" })
    }, 1000);
    return () => {
      clearInterval(id)
    }
  }, [dispatch])

  return (
    <div className='timer'>
      {mins}:{sec}
    </div>
  )
}

export default Timer