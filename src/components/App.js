import Header from "../components/Header";
import Main from "../components/Main";
import Loader from "../components/Loader";
import Error from "../components/Error";
import StartScreen from "../components/StartScreen";
import Questions from './Questions';
import NextButton from './NextButton';
import Finished from './Finished';
import Progress from './Progress';
import Timer from './Timer';
import { useQuiz}  from "../context/QuizContextProvider";

const App = () => {
  const {status}=useQuiz();
  return (
      <div className='app'>
        <Header />
        <Main>
          {
            status === "loading" && <Loader />
          }
          {
            status === "error" && <Error />
          }
          {
            status === "ready" && <StartScreen/>
          }
          {
            status === "active" &&
            <>
              <Progress />
              <Questions />
              <Timer />
              <NextButton />
            </>
          }
          {
            status === "finish" && <Finished />
          }

        </Main>
      </div>

  )
}

export default App
