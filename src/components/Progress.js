import React, { useContext } from 'react'
import { quizContext } from '../context/QuizContextProvider'

const Progress = () => {
    const { index, point, numberOfQuestions, maxPossiblePoints } = useContext(quizContext)
    return (
        <div className=''>
            <progress max={numberOfQuestions} value={index} />
            <p>Questions <strong>{index + 1}</strong> / {numberOfQuestions} </p>
            <p><strong>{point} / {maxPossiblePoints}</strong></p>
        </div>
    )
}

export default Progress


